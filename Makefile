
all:
	cp -r ./lib             $(OUT_DIR)/
	cp -r ./include         $(OUT_DIR)/
	cp -r ./bin             $(OUT_DIR)/
	cp -r ./etc             $(OUT_DIR)/

clean:

remove:
	rm -rf ./lib/*
	rm -rf ./include/*
	rm -rf ./bin/*
